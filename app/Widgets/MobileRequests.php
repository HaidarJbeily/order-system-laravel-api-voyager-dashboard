<?php

namespace   App\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use Arrilot\Widgets\AbstractWidget;

class MobileRequests extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = \App\Models\MobileMaintenanceRequest::count();
        $string = 'Mobile Maintenance Requests';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-phone',
            'title'  => "{$count} {$string}",
            'text'   => "You have {$count} {$string} in database",
            'button' => [
                'text' => 'Mobile Requests',
                'link' => route('voyager.mobile-maintenance-requests.index'),
            ],
            'image' => '/mobiles.jpg',
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', Voyager::model('User'));
    }
}
