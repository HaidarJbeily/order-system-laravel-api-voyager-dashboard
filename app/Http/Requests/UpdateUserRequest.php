<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => ['string'],
            'address' => ['string'],
            'bio' => ['string'],
            'city' => ['string'],
            'email' => ['email', 'unique:users,email'],
            'image' => ['image','mimes:png,jpg,jpeg'],
            'dob' => ['date']
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['success' => false,'errors' => $validator->errors(), 'message' => 'The given data was invalid.'], 422));
    }
}
