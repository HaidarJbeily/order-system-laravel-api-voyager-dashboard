<?php

namespace App\Http\Controllers\BACK;

use TCG\Voyager\Events\BreadDataAdded;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use QrCode;
use DNS2D;


class BarcodeController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request)
    {

        // dd(public_path('storage\\barcodes'));
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        $tmp = $data->toArray();

        // Create Barcode and save it to our path
        $barcode = $data->id;
        $filename = $barcode.".png";
        $t = DNS2D::getBarcodePNG(json_encode($tmp), 'QRCODE');

        Storage::disk('public')->put('barcodes\\'.$filename, base64_decode($t));
        $data->image = "/barcodes/".$filename;
        $data ->save();

        event(new BreadDataAdded($dataType, $data));

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', $data)) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }
}
