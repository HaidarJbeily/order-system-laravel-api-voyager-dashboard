<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\MobileMaintenanceRequest;
use App\Models\GeneralMaintainanceRequest;
use App\Models\CameraAppointment;
use App\Models\Camera;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\API\BaseController;
use App\Http\Requests\OrderGeneralRequest;
use App\Http\Requests\OrderMobileRequest;
use App\Http\Requests\OrderCameraRequest;

use App\Http\Resources\CustomResource;
use  Carbon\Carbon;
class OrderController extends BaseController
{
    // use Controller;

    public function getMyOrders(){
        $customer_id = Auth::user()->customer->id;
        $mobile_requests = MobileMaintenanceRequest::select('id', 'status_id', 'created_at')->where('customer_id', $customer_id)->get();
        $general_requests = GeneralMaintainanceRequest::select('id', 'status_id', 'created_at')->where('customer_id', $customer_id)->get();
        $camera_requests = CameraAppointment::select('id', 'status_id', 'created_at')->where('customer_id', $customer_id)->get();

        $mobile_requests = $mobile_requests->map(function($obj){
            $obj['type'] = 1;
            return $obj;
        })->toArray();

        $general_requests = $general_requests->map(function($obj){
            $obj['type'] = 2;
            return $obj;
        })->toArray();

        $camera_requests = $camera_requests->map(function($obj){
            $obj['type'] = 3;
            return $obj;
        })->toArray();

        $requests = array_merge($mobile_requests, $general_requests, $camera_requests);
        $requests = collect($requests)->sortByDesc('created_at')->values()->all();

        return $this->handleResponse($requests, 'Orders Fetched');

    }

    public function orderGeneral(OrderGeneralRequest $request){
        $data = $request->validated();
        $data['customer_id'] = Auth::user()->customer->id;
        $data['notes'] = $data['notes'] ?? '';
        $order = GeneralMaintainanceRequest::create($data);
        DB::update('update `general_maintainance_requests` set `location` = POINT(?, ?) where id = ?', [$data['long'], $data['lat'], $order->id]);
        $external_ids = array(strval($data['customer_id']));
        $this->sendNotification($external_ids, ['en'=>'General maintenance request is pending now']);
        return $this->handleResponse($order, 'Order created' , 201);
    }

    public function orderMobile(OrderMobileRequest $request){
        $data = $request->validated();
        $data['customer_id'] = Auth::user()->customer->id;
        $data['notes'] = $data['notes'] ?? '';
        $order = MobileMaintenanceRequest::create($data);
        DB::update('update `mobile_maintenance_requests` set `location` = POINT(?, ?) where id = ?', [$data['long'], $data['lat'], $order->id]);
        $external_ids = array(strval($data['customer_id']));
        $this->sendNotification($external_ids, ['en'=>'Mobile maintenance request is pending now']);
        return $this->handleResponse($order, 'Order created' , 201);
    }


    public function orderCamera(OrderCameraRequest $request){
        $data = $request->validated();
        $data['customer_id'] = Auth::user()->customer->id;
        $data['notes'] = $data['notes'] ?? '';
        $camera = Camera::where('id', $request->camera_id)->first();

        $current_datetime =  Carbon::now()->toDateTimeString();

        $last_appointment = CameraAppointment::select('appointment')->where('appointment','>=',$current_datetime)->orderByDesc('appointment')->first()->appointment ?? $current_datetime;
        $installation_time = Carbon::parse($camera->installation_time);
        $last_appointment = Carbon::parse($last_appointment);

        $start = Carbon::createFromTime(9,0,0)->setDate($last_appointment->year, $last_appointment->month, $last_appointment->day);
        $end = Carbon::createFromTime(22,0,0)->setDate($last_appointment->year, $last_appointment->month, $last_appointment->day);
        $data['appointment'] = $last_appointment->addHours($installation_time->hour)->addMinutes($installation_time->minute);
        if($data['appointment']->between($start, $end) == false)
        {
            $data['appointment'] = $start->addDay();
        }
        $data['appointment'] = $data['appointment']->toDateTimeString();
        $order = CameraAppointment::create($data);
        $order->camera = $camera;
        DB::update('update `camera_appointments` set `location` = POINT(?, ?) where id = ?', [$data['long'], $data['lat'], $order->id]);
        $external_ids = array(strval($data['customer_id']));
        $this->sendNotification($external_ids, ['en'=>'Camera appointment is pending now']);
        return $this->handleResponse($order, 'Order created' , 201);
    }

}
