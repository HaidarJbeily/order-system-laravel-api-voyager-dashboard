<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Customer;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Controllers\API\BaseController;

use App\Models\MobileMaintenanceRequest;
use App\Models\GeneralMaintainanceRequest;
use App\Models\CameraAppointment;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UserResource;

class AuthController extends BaseController
{
    public function login(LoginRequest $request)
    {
        if(Auth::attempt(['phone' => $request->phone, 'password' => $request->password])){
            $auth = Auth::user();
            $customer = $auth->customer;
            $auth->token = $auth->createToken('view-token')->plainTextToken;
            $customer->mobileOrdersNumber = MobileMaintenanceRequest::where('customer_id', $customer->id)->count();
            $customer->generalOrdersNumber = GeneralMaintainanceRequest::where('customer_id', $customer->id)->count();
            $customer->cameraOrdersNumber = CameraAppointment::where('customer_id', $customer->id)->count();
            $success = new UserResource($auth);
            return $this->handleResponse($success, 'User logged-in!');
        }

        return $this->handleError('Unauthorized.', ['phone'=>'phone number or password is incorrect'], 401);


    }

    public function register(RegisterRequest $request)
    {
        $input = $request->validated();
        $input['password'] = bcrypt($input['password']);
        $input['name'] = $input['username'];
        $input['phone_number'] = $input['phone'];

        $user = User::create($input);

        $input['user_id'] = $user->id;
        $customer = Customer::create($input);

        $user->token = $user->createToken('view-token')->plainTextToken;
        $user->customer()->save($customer);
        $user->customer->mobileOrdersNumber = 0;
        $user->customer->generalOrdersNumber = 0;
        $user->customer->cameraOrdersNumber = 0;
        $success = new UserResource($user);
        return $this->handleResponse($success, 'User Created Successfully!', 201);
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return $this->handleResponse([], 'Logout completed!');
    }

    public function getUser(Request $request)
    {
        $user = Auth::user();
        $customer = $user->customer;
        $customer->mobileOrdersNumber = MobileMaintenanceRequest::where('customer_id', $customer->id)->count();
        $customer->generalOrdersNumber = GeneralMaintainanceRequest::where('customer_id', $customer->id)->count();
        $customer->cameraOrdersNumber = CameraAppointment::where('customer_id', $customer->id)->count();
        return $this->handleResponse(new UserResource($user), 'Profile Data Fetched');
    }

    public function updateUser(UpdateUserRequest $request)
    {
        $user = Auth::user();
        $customer = $user->customer;
        $data = $request->validated();

        $user->update($data);
        $customer->update($data);
        if(!is_null($data['image']))
        {
            $image_name =time().'_'.$data['image']->getClientOriginalName();
            $data['image']->storeAs('public/profiles',$image_name);

            $customer->image = "profiles/".$image_name;
            $customer->save();
        }
        $customer->mobileOrdersNumber = MobileMaintenanceRequest::where('customer_id', $customer->id)->count();
        $customer->generalOrdersNumber = GeneralMaintainanceRequest::where('customer_id', $customer->id)->count();
        $customer->cameraOrdersNumber = CameraAppointment::where('customer_id', $customer->id)->count();
        return $this->handleResponse(new UserResource($user), 'Profile has been Updated');
    }
}
