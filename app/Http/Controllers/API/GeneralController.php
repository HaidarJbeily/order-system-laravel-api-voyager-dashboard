<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Auth;
use App\Models\Camera;
use App\Models\Supplier;
use App\Models\Notification;
use App\Models\StickerBarcodeInfo;
use App\Models\Bundle;
use Illuminate\Http\Request;
use App\Http\Resources\CustomResource;
use App\Http\Controllers\API\BaseController;

class GeneralController extends BaseController
{
    public function getAllCameras()
    {
        $cameras = Camera::all();
        return $this->handleResponse(new CustomResource($cameras), 'Cameras fetched!');
    }

    public function getAllBundles($sup_id)
    {
        $bundles = Bundle::where('supplier_id', $sup_id)->get();
        return $this->handleResponse(new CustomResource($bundles), 'Bundles fetched!');
    }

    public function getAllSuppliers()
    {
        $suppliers = Supplier::all();
        return $this->handleResponse(new CustomResource($suppliers), 'Suppliers fetched!');
    }

    public function getMyBarCodes()
    {
        $customer_id = Auth::user()->customer->id;
        $barCodes = StickerBarcodeInfo::where('customer_id', $customer_id)->get();
        return $this->handleResponse(new CustomResource($barCodes), 'BarCodes fetched!');
    }

    public function getMyNotifications()
    {
        $customer_id = Auth::user()->customer->id;
        $notification = Notification::where('customer_id', $customer_id)->orWhereNull('customer_id')->orderByDesc('created_at')->get();
        return $this->handleResponse(new CustomResource($notification), 'Notifications fetched!');
    }

}
