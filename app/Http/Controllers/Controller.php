<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use GuzzleHttp\Client;
use App\Models\Notification;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendNotification($external_ids, $message)
    {
        $ONE_SIGNAL_BASE_URL = 'https://onesignal.com/api/v1/notifications';
        $ONE_SIGNAL_APP_ID = '9168d169-307f-4831-a729-5117ab973a06';
        $ONE_SIGNAL_API_KEY = 'OTNiNmM3M2YtMDg4ZC00ZjAyLWE2ZjYtYWIzY2NjNzc2OWEz';

        $body = json_encode(
             [
                "include_external_user_ids" => $external_ids,
                "app_id" => $ONE_SIGNAL_APP_ID,
                "headings" => ["en" => 'View'],
                "contents" => $message,
                "data" => ["foo"=>'bar'],
            ]);

        $client = new Client(['headers' => ['Authorization' => 'Basic '.$ONE_SIGNAL_API_KEY, 'Content-Type' => 'application/json']]);

        $request = $client->post($ONE_SIGNAL_BASE_URL, ['body' => $body]);
        foreach($external_ids as $key => $id)
        {
            Notification::create(['customer_id' => intval($id), 'title' => 'View', 'text' => $message['en']]);
        }
    }
}
