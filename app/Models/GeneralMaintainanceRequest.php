<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Spatial;

class GeneralMaintainanceRequest extends Model
{
    use HasFactory;

    use Spatial;

    protected $spatial = ['location'];

    protected $fillable = [
        'customer_id',
        'phone',
        'location',
        'notes',
    ];

    public $table = 'general_maintainance_requests';
}
