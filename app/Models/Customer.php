<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Spatial;
use App\Models\User;


class Customer extends Model
{
    use HasFactory, Spatial;

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'location',
        'address',
        'username',
        'rank',
        'image',
        'bio',
        'dob',
        'city',
        'phone_number',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
