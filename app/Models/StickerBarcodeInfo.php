<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\StickerInfo;


class StickerBarcodeInfo extends Model
{
    use HasFactory;

    public $timestamps = false;



    /**
     * Get the sticker that owns the StickerBarcodeInfo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sticker()
    {
        return $this->belongsTo(StickerInfo::class);
    }
}
