<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Spatial;

class CameraAppointment extends Model
{
    use HasFactory;

    use Spatial;

    protected $spatial = ['location'];

    protected $fillable = [
        'customer_id',
        'phone',
        'notes',
        'camera_id',
        'appointment',
    ];


    /**
     * Get the user that owns the CameraAppointment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function camera()
    {
        return $this->belongsTo(Camera::class);
    }
}
