<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\GeneralController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


// Route::middleware('auth:sanctum')->post('/login', [AuthController::class, 'login']);

Route::group(['middleware' => ['apiAuth', 'auth:sanctum']],function () {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::get('user', [AuthController::class, 'getUser']);
    Route::put('user', [AuthController::class, 'updateUser']);

    Route::get('orders', [OrderController::class, 'getMyOrders']);
    Route::post('orders/general', [OrderController::class, 'orderGeneral']);
    Route::post('orders/mobile', [OrderController::class, 'orderMobile']);
    Route::post('orders/camera', [OrderController::class, 'orderCamera']);

    Route::get('/barcodes', [GeneralController::class, 'getMyBarCodes']);

    Route::get('/notifications', [GeneralController::class, 'getMyNotifications']);
});

Route::post('/login', [AuthController::class,'login']);
Route::post('/register', [AuthController::class,'register']);

Route::get('/suppliers/{sup_id}/bundles', [GeneralController::class, 'getAllBundles']);
Route::get('/suppliers', [GeneralController::class, 'getAllSuppliers']);
Route::get('/cameras', [GeneralController::class, 'getAllCameras']);

