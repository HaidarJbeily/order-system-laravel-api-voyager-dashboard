-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307:8111
-- Generation Time: Jan 10, 2022 at 11:13 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `view`
--

-- --------------------------------------------------------

--
-- Table structure for table `bundles`
--

CREATE TABLE `bundles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `section_id` int(10) UNSIGNED NOT NULL,
  `price` int(11) NOT NULL,
  `bundle_size` int(11) NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_available` bit(1) NOT NULL,
  `is_available` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bundles`
--

INSERT INTO `bundles` (`id`, `name`, `description`, `supplier_id`, `section_id`, `price`, `bundle_size`, `picture`, `ranking`, `note`, `discount_available`, `is_available`) VALUES
(2, 'Haidar Jbeily', 'qwqw', 2, 2, 1212, 12, 'bundles\\December2021\\DkO3AOjaY7tLQ76R0hvP.jpg', 2, '1121', b'1', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cameras`
--

CREATE TABLE `cameras` (
  `id` int(10) UNSIGNED NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sensor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resolution` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost` float NOT NULL,
  `price` float NOT NULL,
  `manufacturer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rank` int(11) DEFAULT NULL,
  `is_available` bit(1) DEFAULT NULL,
  `image` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `installation_time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cameras`
--

INSERT INTO `cameras` (`id`, `model`, `sensor`, `resolution`, `cost`, `price`, `manufacturer`, `rank`, `is_available`, `image`, `installation_time`) VALUES
(2, 'Ea ut ex alias in du', 'Dolore voluptatem v', 'Commodo non voluptat', 70, 514, 'Ducimus molestiae e', 90, b'1', 'cameras\\December2021\\fC01qWfBlmruoYS8n04o.jpg', '03:06:00');

-- --------------------------------------------------------

--
-- Table structure for table `camera_appointments`
--

CREATE TABLE `camera_appointments` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `camera_id` int(10) UNSIGNED NOT NULL,
  `appointment` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `location` point DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `status_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `camera_appointments`
--

INSERT INTO `camera_appointments` (`id`, `customer_id`, `camera_id`, `appointment`, `location`, `notes`, `created_at`, `updated_at`, `status_id`, `phone`) VALUES
(66, 8, 2, '2022-01-09 22:16:24', 0x000000000101000000917efb3a70564740f46c567daeb63840, '', '2022-01-09 20:16:24', '2022-01-09 20:16:24', 1, '0997610306');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `rank` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `city` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `user_id`, `rank`, `location`, `username`, `image`, `bio`, `address`, `dob`, `city`, `phone_number`) VALUES
(4, 7, NULL, NULL, 'haidar', NULL, NULL, NULL, NULL, NULL, '0997610306'),
(6, 9, NULL, NULL, 'haidar11', NULL, NULL, NULL, NULL, NULL, '0988610306'),
(7, 10, NULL, NULL, 'HaidarJbeily', NULL, NULL, NULL, NULL, NULL, '0998877661'),
(8, 11, NULL, NULL, 'Haidar1Jbeily', 'profiles\\1641468906_avatar.jpg', 'here I am', 'St. 211', '2000-09-30', 'Alryyad', '09948877661'),
(9, 12, NULL, NULL, 'Haidar1Jb', NULL, NULL, NULL, NULL, NULL, '0994887766'),
(10, 13, NULL, NULL, 'Haidar1Jb1', NULL, NULL, NULL, NULL, NULL, '0994887764'),
(11, 14, NULL, NULL, 'Haidar1Jb11', NULL, NULL, NULL, NULL, NULL, '0994887762'),
(12, 15, NULL, NULL, 'Haidar1Jb10', NULL, NULL, NULL, NULL, NULL, '0994887765'),
(13, 16, NULL, NULL, 'Haidar12', NULL, NULL, NULL, NULL, NULL, '0994887763'),
(14, 17, NULL, NULL, 'Haidar120', NULL, NULL, NULL, NULL, NULL, '09948877612'),
(15, 18, NULL, NULL, 'yy', NULL, NULL, NULL, NULL, NULL, '0999999991');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 4),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 5),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 6),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 7),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 11),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(8, 1, 'avatar', 'image', 'Avatar', 0, 0, 1, 1, 1, 1, '{}', 15),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Additional Role', 0, 0, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 3),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 14),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 13),
(22, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 5),
(24, 6, 'description', 'text', 'Description', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 6),
(25, 6, 'supplier_id', 'text', 'Supplier Id', 1, 1, 1, 1, 1, 1, '{}', 7),
(26, 6, 'section_id', 'text', 'Section Id', 1, 1, 1, 1, 1, 1, '{}', 8),
(27, 6, 'price', 'number', 'Price', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|numeric\"}}', 9),
(28, 6, 'bundle_size', 'number', 'Bundle Size', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|numeric\"}}', 10),
(29, 6, 'picture', 'image', 'Picture', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"image|mimes:png,jpg,jpeg\"}}', 11),
(30, 6, 'ranking', 'number', 'Ranking', 0, 0, 0, 0, 0, 0, '{}', 12),
(31, 6, 'note', 'text', 'Note', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"string\"}}', 13),
(32, 6, 'discount_available', 'checkbox', 'Discount Available', 1, 0, 0, 0, 0, 0, '{}', 14),
(33, 6, 'is_available', 'checkbox', 'Is Available', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 15),
(34, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(35, 8, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 3),
(36, 8, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 4),
(37, 8, 'location', 'text', 'Location', 0, 0, 0, 0, 0, 0, '{}', 5),
(38, 8, 'contact_number', 'text', 'Contact Number', 0, 1, 1, 1, 1, 1, '{}', 6),
(39, 8, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|email\"}}', 7),
(40, 8, 'contact_name', 'text', 'Contact Name', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 8),
(41, 6, 'bundle_belongsto_section_relationship', 'relationship', 'Section', 1, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Section\",\"table\":\"sections\",\"type\":\"belongsTo\",\"column\":\"section_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(42, 8, 'section_hasmany_bundle_relationship', 'relationship', 'Bundle', 1, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Bundle\",\"table\":\"bundles\",\"type\":\"hasMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(43, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(44, 12, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 2),
(45, 12, 'contact_first_name', 'text', 'Contact First Name', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 3),
(46, 12, 'contact_last_name', 'text', 'Contact Last Name', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 4),
(47, 12, 'address', 'text', 'Address', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 5),
(48, 12, 'city', 'text', 'City', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 6),
(49, 12, 'postal_code', 'number', 'Postal Code', 0, 1, 1, 1, 1, 1, '{}', 7),
(50, 12, 'country', 'text', 'Country', 1, 1, 1, 1, 1, 1, '{}', 8),
(51, 12, 'phone', 'number', 'Phone', 0, 1, 1, 1, 1, 1, '{}', 9),
(52, 12, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 10),
(53, 12, 'fax', 'number', 'Fax', 0, 1, 1, 1, 1, 1, '{}', 11),
(54, 12, 'url', 'text', 'Url', 1, 1, 1, 1, 1, 1, '{}', 12),
(55, 12, 'type_goods', 'text', 'Type Goods', 0, 1, 1, 1, 1, 1, '{}', 13),
(56, 12, 'notes', 'text', 'Notes', 0, 1, 1, 1, 1, 1, '{}', 14),
(57, 12, 'logo', 'image', 'Logo', 1, 1, 1, 1, 1, 1, '{}', 15),
(58, 12, 'discount_available', 'checkbox', 'Discount Available', 0, 1, 1, 1, 1, 1, 'null', 16),
(59, 6, 'bundle_belongsto_supplier_relationship', 'relationship', 'Supplier', 1, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Supplier\",\"table\":\"suppliers\",\"type\":\"belongsTo\",\"column\":\"supplier_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(60, 13, 'product_id', 'text', 'Product Id', 1, 0, 0, 0, 0, 0, '{}', 2),
(61, 13, 'discount', 'number', 'Discount', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|numeric\"}}', 4),
(62, 13, 'description', 'text', 'Description', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 5),
(63, 13, 'start_date', 'timestamp', 'Start Date', 0, 1, 1, 1, 1, 1, '{}', 6),
(64, 13, 'end_date', 'timestamp', 'End Date', 0, 1, 1, 1, 1, 1, '{}', 7),
(65, 13, 'deal_hasone_bundle_relationship', 'relationship', 'bundles', 1, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Bundle\",\"table\":\"bundles\",\"type\":\"belongsTo\",\"column\":\"product_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(66, 6, 'bundle_hasone_deal_relationship', 'relationship', 'Deal', 1, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Deal\",\"table\":\"deals\",\"type\":\"hasOne\",\"column\":\"product_id\",\"key\":\"id\",\"label\":\"product_id\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(67, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(68, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(69, 14, 'user_id', 'text', 'User Id', 1, 1, 1, 0, 0, 1, '{}', 2),
(70, 14, 'rank', 'number', 'Rank', 0, 0, 0, 0, 0, 0, '{}', 7),
(71, 14, 'location', 'text', 'Location', 0, 1, 1, 0, 0, 1, '{}', 10),
(72, 14, 'username', 'text', 'Username', 1, 1, 1, 1, 1, 1, '{}', 4),
(73, 14, 'image', 'image', 'Image', 0, 0, 1, 1, 1, 1, '{}', 12),
(74, 14, 'bio', 'text', 'Bio', 0, 0, 0, 0, 0, 0, '{}', 6),
(75, 14, 'address', 'text', 'Address', 0, 1, 1, 1, 1, 1, '{}', 9),
(76, 14, 'dob', 'date', 'Date of Birth', 0, 1, 1, 1, 1, 1, '{}', 11),
(77, 14, 'city', 'text', 'City', 0, 1, 1, 1, 1, 1, '{}', 8),
(79, 14, 'customer_hasone_user_relationship', 'relationship', 'User', 0, 1, 1, 0, 0, 0, '{\"model\":\"App\\\\Models\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(80, 1, 'user_hasone_customer_relationship', 'relationship', 'Customer', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Customer\",\"table\":\"customers\",\"type\":\"hasOne\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"username\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(81, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(82, 15, 'customer_id', 'text', 'Customer Id', 1, 1, 1, 0, 0, 0, '{}', 2),
(84, 15, 'notes', 'text', 'Notes', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 9),
(85, 15, 'general_maintainance_request_belongsto_customer_relationship', 'relationship', 'Customer', 1, 1, 1, 0, 0, 0, '{\"model\":\"App\\\\Models\\\\Customer\",\"table\":\"customers\",\"type\":\"belongsTo\",\"column\":\"customer_id\",\"key\":\"id\",\"label\":\"username\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(86, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(87, 16, 'customer_id', 'text', 'Customer Id', 1, 1, 1, 0, 0, 0, '{}', 2),
(88, 16, 'location', 'coordinates', 'Location', 0, 0, 1, 0, 0, 0, '{}', 5),
(89, 16, 'location_type', 'text', 'Location Type', 0, 0, 0, 0, 0, 0, '{}', 6),
(90, 16, 'status_id', 'text', 'Status Id', 1, 1, 1, 1, 1, 1, '{}', 7),
(91, 16, 'notes', 'text', 'Notes', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"string\"}}', 11),
(92, 16, 'due_date', 'timestamp', 'Due Date', 0, 0, 0, 0, 0, 0, '{}', 9),
(93, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(94, 17, 'model', 'text', 'Model', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 2),
(95, 17, 'sensor', 'text', 'Sensor', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 3),
(96, 17, 'resolution', 'text', 'Resolution', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 4),
(97, 17, 'cost', 'number', 'Cost', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|numeric\"}}', 5),
(98, 17, 'price', 'number', 'Price', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|numeric\"}}', 6),
(99, 17, 'manufacturer', 'text', 'Manufacturer', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 7),
(100, 17, 'rank', 'number', 'Rank', 0, 0, 0, 0, 0, 0, '{}', 9),
(101, 17, 'is_available', 'checkbox', 'Is Available', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 10),
(102, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(103, 18, 'customer_id', 'text', 'Customer Id', 1, 1, 1, 0, 0, 0, '{}', 4),
(104, 18, 'camera_id', 'text', 'Camera Id', 1, 1, 1, 0, 0, 0, '{}', 6),
(105, 18, 'appointment', 'timestamp', 'Appointment', 1, 1, 1, 1, 1, 0, '{}', 8),
(106, 18, 'location', 'coordinates', 'Location', 0, 0, 1, 0, 0, 0, '{}', 9),
(107, 18, 'notes', 'text', 'Notes', 0, 1, 1, 1, 1, 0, '{}', 11),
(108, 18, 'camera_appointment_belongsto_camera_relationship', 'relationship', 'camera', 1, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Camera\",\"table\":\"cameras\",\"type\":\"belongsTo\",\"column\":\"camera_id\",\"key\":\"id\",\"label\":\"model\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(109, 18, 'camera_appointment_belongsto_customer_relationship', 'relationship', 'Customer', 1, 1, 1, 0, 0, 0, '{\"model\":\"App\\\\Models\\\\Customer\",\"table\":\"customers\",\"type\":\"belongsTo\",\"column\":\"customer_id\",\"key\":\"id\",\"label\":\"username\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(118, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(119, 23, 'customer_id', 'text', 'Customer Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(121, 23, 'applied_at', 'timestamp', 'Applied At', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 5),
(122, 23, 'expired_at', 'timestamp', 'Expired At', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 6),
(123, 23, 'device_type', 'text', 'Device Type', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 7),
(124, 23, 'serial_number', 'text', 'Serial Number', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 9),
(125, 23, 'sticker_barcode_info_belongsto_customer_relationship', 'relationship', 'Customer', 1, 1, 1, 1, 1, 0, '{\"model\":\"App\\\\Models\\\\Customer\",\"table\":\"customers\",\"type\":\"belongsTo\",\"column\":\"customer_id\",\"key\":\"id\",\"label\":\"username\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(127, 17, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|image|mimes:png,jpg,jpeg|max:8192\"}}', 11),
(128, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 0, 0, 0, 0, 0, '{}', 8),
(129, 1, 'phone', 'text', 'Phone Number', 0, 1, 1, 0, 0, 0, '{}', 10),
(130, 14, 'phone_number', 'text', 'Phone Number', 1, 0, 0, 0, 0, 0, '{}', 11),
(131, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 8),
(132, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(133, 15, 'status_id', 'number', 'Status Id', 1, 1, 1, 0, 0, 0, '{}', 5),
(134, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 10),
(135, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(136, 18, 'status_id', 'number', 'Status Id', 1, 1, 1, 0, 0, 0, '{}', 12),
(137, 23, 'image', 'image', 'Image', 0, 1, 1, 0, 0, 0, '{}', 13),
(138, 16, 'mobile_maintenance_request_belongsto_customer_relationship', 'relationship', 'Customer', 1, 1, 1, 0, 0, 0, '{\"model\":\"App\\\\Models\\\\Customer\",\"table\":\"customers\",\"type\":\"belongsTo\",\"column\":\"customer_id\",\"key\":\"id\",\"label\":\"username\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(139, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 10),
(140, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(141, 16, 'phone', 'text', 'Phone', 1, 1, 1, 0, 0, 0, '{}', 4),
(142, 25, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(144, 25, 'title', 'text', 'Title', 1, 1, 1, 0, 1, 0, '{\"validation\":{\"rule\":\"required|string\"}}', 4),
(145, 25, 'text', 'text', 'Text', 1, 1, 1, 0, 1, 0, '{\"validation\":{\"rule\":\"required|string\"}}', 5),
(146, 25, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 6),
(147, 25, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(148, 25, 'notification_belongsto_user_relationship', 'relationship', 'Customer', 1, 0, 0, 0, 0, 0, '{\"model\":\"App\\\\Models\\\\Customer\",\"table\":\"customers\",\"type\":\"belongsTo\",\"column\":\"customer_id\",\"key\":\"id\",\"label\":\"username\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(149, 25, 'customer_id', 'text', 'Customer Id', 0, 0, 0, 0, 0, 0, '{}', 2),
(150, 18, 'camera_appointment_hasone_status_relationship', 'relationship', 'Status', 1, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Status\",\"table\":\"statuses\",\"type\":\"belongsTo\",\"column\":\"status_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(151, 18, 'phone', 'text', 'Phone', 1, 1, 1, 0, 0, 0, '{\"validation\":{\"rule\":[\"required\"]}}', 5),
(152, 15, 'general_maintainance_request_hasone_status_relationship', 'relationship', 'Status', 1, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Status\",\"table\":\"statuses\",\"type\":\"belongsTo\",\"column\":\"status_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 6),
(153, 15, 'phone', 'text', 'Phone', 1, 1, 1, 0, 1, 0, '{}', 4),
(154, 15, 'location', 'coordinates', 'Location', 0, 0, 1, 0, 0, 0, '{}', 7),
(155, 16, 'mobile_maintenance_request_belongsto_status_relationship', 'relationship', 'Status', 1, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Status\",\"table\":\"statuses\",\"type\":\"belongsTo\",\"column\":\"status_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bundles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(156, 17, 'installation_time', 'time', 'Installation Time', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 8),
(157, 23, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 4),
(158, 23, 'description', 'text', 'Description', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 8),
(159, 23, 'model', 'text', 'Model', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string\"}}', 10),
(160, 23, 'cost', 'text', 'Cost', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|numeric\"}}', 11),
(161, 23, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|numeric\"}}', 12),
(162, 23, 'warranty_days', 'number', 'Warranty Days', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|integer\"}}', 13);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2021-12-17 11:41:49', '2021-12-22 14:32:38'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2021-12-17 11:41:49', '2021-12-22 17:10:05'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2021-12-17 11:41:49', '2021-12-17 11:41:49'),
(6, 'bundles', 'bundles', 'Bundle', 'Bundles', 'voyager-treasure', 'App\\Models\\Bundle', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-12-19 21:54:15', '2022-01-10 07:37:20'),
(8, 'sections', 'sections', 'Section', 'Sections', 'voyager-book', 'App\\Models\\Section', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-12-19 22:23:11', '2022-01-10 08:01:26'),
(12, 'suppliers', 'suppliers', 'Supplier', 'Suppliers', 'voyager-basket', 'App\\Models\\Supplier', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-12-19 23:06:40', '2022-01-10 08:06:53'),
(13, 'deals', 'deals', 'Deal', 'Deals', 'voyager-medal-rank-star', 'App\\Models\\Deal', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2021-12-20 08:12:07', '2022-01-10 07:55:14'),
(14, 'customers', 'customers', 'Customer', 'Customers', 'voyager-people', 'App\\Models\\Customer', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-12-20 08:46:04', '2022-01-10 07:51:12'),
(15, 'general_maintainance_requests', 'general-maintainance-requests', 'General Maintainance Request', 'General Maintainance Requests', 'voyager-window-list', 'App\\Models\\GeneralMaintainanceRequest', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-12-20 08:57:45', '2022-01-10 07:56:05'),
(16, 'mobile_maintenance_requests', 'mobile-maintenance-requests', 'Mobile Maintenance Request', 'Mobile Maintenance Requests', 'voyager-phone', 'App\\Models\\MobileMaintenanceRequest', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-12-20 09:23:34', '2022-01-10 07:57:57'),
(17, 'cameras', 'cameras', 'Camera', 'Cameras', 'voyager-camera', 'App\\Models\\Camera', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-12-20 09:27:38', '2022-01-10 07:50:02'),
(18, 'camera_appointments', 'camera-appointments', 'Camera Appointment', 'Camera Appointments', 'voyager-calendar', 'App\\Models\\CameraAppointment', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-12-20 09:32:35', '2022-01-10 07:41:16'),
(21, 'sticker_info', 'sticker-info', 'Sticker Info', 'Sticker Info', 'voyager-star-two', 'App\\Models\\StickerInfo', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-12-20 09:48:37', '2021-12-20 09:48:37'),
(23, 'sticker_barcode_infos', 'sticker-barcode-infos', 'Sticker Barcode Info', 'Sticker Barcode Infos', 'voyager-company', 'App\\Models\\StickerBarcodeInfo', NULL, 'App\\Http\\Controllers\\BACK\\BarcodeController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-12-20 10:01:07', '2022-01-10 08:11:08'),
(25, 'notifications', 'notifications', 'Notification', 'Notifications', 'voyager-bell', 'App\\Models\\Notification', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-12-24 21:53:05', '2022-01-10 07:58:35');

-- --------------------------------------------------------

--
-- Table structure for table `deals`
--

CREATE TABLE `deals` (
  `id` int(10) UNSIGNED NOT NULL,
  `discount` float NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `general_maintainance_requests`
--

CREATE TABLE `general_maintainance_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `status_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` point DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `general_maintainance_requests`
--

INSERT INTO `general_maintainance_requests` (`id`, `customer_id`, `notes`, `created_at`, `updated_at`, `status_id`, `phone`, `location`) VALUES
(6, 8, '', '2022-01-08 13:15:23', '2022-01-08 13:15:23', 1, '0997610306', 0x0000000001010000003b014d840d1f3640cdcccccccc4c3640),
(7, 8, '', '2022-01-08 13:17:08', '2022-01-08 13:17:08', 1, '0997610306', 0x00000000010100000000000000000024409a99999999992440);

-- --------------------------------------------------------

--
-- Table structure for table `guarantee_types`
--

CREATE TABLE `guarantee_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `times_to_apply` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `guarantee_types_sticker_info`
--

CREATE TABLE `guarantee_types_sticker_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `guarantee_type_id` int(10) UNSIGNED NOT NULL,
  `sticker_info_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2021-12-17 11:41:51', '2021-12-17 11:41:51');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2021-12-17 11:41:51', '2021-12-17 11:41:51', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 3, '2021-12-17 11:41:51', '2021-12-20 09:14:51', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, 18, 1, '2021-12-17 11:41:51', '2021-12-20 09:06:31', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2021-12-17 11:41:51', '2021-12-17 11:41:51', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 4, '2021-12-17 11:41:51', '2021-12-20 09:14:51', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2021-12-17 11:41:51', '2021-12-20 08:22:19', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2021-12-17 11:41:51', '2021-12-20 08:22:19', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2021-12-17 11:41:52', '2021-12-20 08:22:19', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2021-12-17 11:41:52', '2021-12-20 08:22:19', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 5, '2021-12-17 11:41:52', '2021-12-20 09:14:51', 'voyager.settings.index', NULL),
(12, 1, 'Bundles', '', '_self', 'voyager-treasure', '#000000', 20, 1, '2021-12-19 21:54:15', '2021-12-20 09:14:28', 'voyager.bundles.index', 'null'),
(13, 1, 'Sections', '', '_self', 'voyager-book', '#000000', 20, 2, '2021-12-19 22:23:11', '2021-12-20 09:14:35', 'voyager.sections.index', 'null'),
(14, 1, 'Suppliers', '', '_self', 'voyager-basket', '#000000', 20, 3, '2021-12-19 23:06:40', '2021-12-20 09:14:35', 'voyager.suppliers.index', 'null'),
(15, 1, 'Deals', '', '_self', 'voyager-medal-rank-star', '#000000', 20, 4, '2021-12-20 08:12:08', '2021-12-20 09:14:40', 'voyager.deals.index', 'null'),
(16, 1, 'Customers', '', '_self', 'voyager-people', NULL, 18, 2, '2021-12-20 08:46:04', '2021-12-20 09:06:37', 'voyager.customers.index', NULL),
(17, 1, 'General Maintainance', '', '_self', 'voyager-window-list', '#000000', 19, 1, '2021-12-20 08:57:45', '2021-12-20 09:15:17', 'voyager.general-maintainance-requests.index', 'null'),
(18, 1, 'Members', '', '_self', 'voyager-people', '#000000', NULL, 6, '2021-12-20 09:06:24', '2021-12-20 09:14:51', NULL, ''),
(19, 1, 'Orders', '', '_self', 'voyager-shop', '#000000', NULL, 7, '2021-12-20 09:13:45', '2021-12-20 09:17:48', NULL, ''),
(20, 1, 'Info', '', '_self', 'voyager-news', '#000000', NULL, 8, '2021-12-20 09:14:20', '2021-12-20 09:17:17', NULL, ''),
(21, 1, 'Mobile Maintenance', '', '_self', 'voyager-phone', '#000000', 19, 2, '2021-12-20 09:23:34', '2021-12-20 09:24:54', 'voyager.mobile-maintenance-requests.index', 'null'),
(22, 1, 'Cameras', '', '_self', 'voyager-camera', NULL, 20, 5, '2021-12-20 09:27:38', '2021-12-20 09:27:50', 'voyager.cameras.index', NULL),
(23, 1, 'Camera Appointments', '', '_self', 'voyager-calendar', NULL, 19, 3, '2021-12-20 09:32:35', '2021-12-20 09:33:09', 'voyager.camera-appointments.index', NULL),
(28, 1, 'Sticker Barcode', '', '_self', 'voyager-company', '#000000', 20, 6, '2021-12-20 10:01:08', '2022-01-09 19:44:43', 'voyager.sticker-barcode-infos.index', 'null'),
(29, 1, 'Notifications', '', '_self', 'voyager-bell', '#000000', NULL, 9, '2021-12-24 21:53:06', '2021-12-24 21:59:33', 'voyager.notifications.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(25, '2021_12_20_014433_create_bundles_table', 0),
(26, '2021_12_20_014433_create_camera_appointments_table', 0),
(27, '2021_12_20_014433_create_cameras_table', 0),
(28, '2021_12_20_014433_create_customers_table', 0),
(29, '2021_12_20_014433_create_data_rows_table', 0),
(30, '2021_12_20_014433_create_data_types_table', 0),
(31, '2021_12_20_014433_create_deals_table', 0),
(32, '2021_12_20_014433_create_failed_jobs_table', 0),
(33, '2021_12_20_014433_create_general_maintainance_requests_table', 0),
(34, '2021_12_20_014433_create_guarantee_types_table', 0),
(35, '2021_12_20_014433_create_guarantee_types_sticker_info_table', 0),
(36, '2021_12_20_014433_create_menu_items_table', 0),
(37, '2021_12_20_014433_create_menus_table', 0),
(38, '2021_12_20_014433_create_mobile_maintenance_requests_table', 0),
(39, '2021_12_20_014433_create_notifications_table', 0),
(40, '2021_12_20_014433_create_password_resets_table', 0),
(41, '2021_12_20_014433_create_permission_role_table', 0),
(42, '2021_12_20_014433_create_permissions_table', 0),
(43, '2021_12_20_014433_create_personal_access_tokens_table', 0),
(44, '2021_12_20_014433_create_roles_table', 0),
(45, '2021_12_20_014433_create_sections_table', 0),
(46, '2021_12_20_014433_create_settings_table', 0),
(47, '2021_12_20_014433_create_status_table', 0),
(48, '2021_12_20_014433_create_sticker_barcode_info_table', 0),
(49, '2021_12_20_014433_create_sticker_info_table', 0),
(50, '2021_12_20_014433_create_suppliers_table', 0),
(51, '2021_12_20_014433_create_translations_table', 0),
(52, '2021_12_20_014433_create_user_roles_table', 0),
(53, '2021_12_20_014433_create_users_table', 0),
(54, '2021_12_20_014434_add_foreign_keys_to_data_rows_table', 0),
(55, '2021_12_20_014434_add_foreign_keys_to_menu_items_table', 0),
(56, '2021_12_20_014434_add_foreign_keys_to_permission_role_table', 0),
(57, '2021_12_20_014434_add_foreign_keys_to_user_roles_table', 0),
(58, '2021_12_20_014434_add_foreign_keys_to_users_table', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mobile_maintenance_requests`
--

CREATE TABLE `mobile_maintenance_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `location` point DEFAULT NULL,
  `location_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `due_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mobile_maintenance_requests`
--

INSERT INTO `mobile_maintenance_requests` (`id`, `customer_id`, `location`, `location_type`, `status_id`, `notes`, `due_date`, `created_at`, `updated_at`, `phone`) VALUES
(2, 8, 0x00000000010100000000000000000036400000000000002640, NULL, 1, '', NULL, '2022-01-09 13:21:19', '2022-01-09 13:21:19', '0997610306');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `customer_id`, `title`, `text`, `created_at`, `updated_at`) VALUES
(1, 8, 'Hello', 'Test', '2021-12-24 22:09:00', '2021-12-24 22:09:00'),
(2, 8, 'View', 'Camera appointment is pending now', '2022-01-09 12:50:18', '2022-01-09 12:50:18'),
(3, NULL, 'public title', 'public text', '2022-01-09 13:11:34', '2022-01-09 13:11:34'),
(4, NULL, 'public title', 'public text', '2022-01-09 13:11:34', '2022-01-09 13:11:34');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2021-12-17 11:41:52', '2021-12-17 11:41:52'),
(2, 'browse_bread', NULL, '2021-12-17 11:41:52', '2021-12-17 11:41:52'),
(3, 'browse_database', NULL, '2021-12-17 11:41:52', '2021-12-17 11:41:52'),
(4, 'browse_media', NULL, '2021-12-17 11:41:52', '2021-12-17 11:41:52'),
(5, 'browse_compass', NULL, '2021-12-17 11:41:53', '2021-12-17 11:41:53'),
(6, 'browse_menus', 'menus', '2021-12-17 11:41:53', '2021-12-17 11:41:53'),
(7, 'read_menus', 'menus', '2021-12-17 11:41:53', '2021-12-17 11:41:53'),
(8, 'edit_menus', 'menus', '2021-12-17 11:41:53', '2021-12-17 11:41:53'),
(9, 'add_menus', 'menus', '2021-12-17 11:41:53', '2021-12-17 11:41:53'),
(10, 'delete_menus', 'menus', '2021-12-17 11:41:53', '2021-12-17 11:41:53'),
(11, 'browse_roles', 'roles', '2021-12-17 11:41:53', '2021-12-17 11:41:53'),
(12, 'read_roles', 'roles', '2021-12-17 11:41:54', '2021-12-17 11:41:54'),
(13, 'edit_roles', 'roles', '2021-12-17 11:41:54', '2021-12-17 11:41:54'),
(14, 'add_roles', 'roles', '2021-12-17 11:41:54', '2021-12-17 11:41:54'),
(15, 'delete_roles', 'roles', '2021-12-17 11:41:54', '2021-12-17 11:41:54'),
(16, 'browse_users', 'users', '2021-12-17 11:41:54', '2021-12-17 11:41:54'),
(17, 'read_users', 'users', '2021-12-17 11:41:54', '2021-12-17 11:41:54'),
(18, 'edit_users', 'users', '2021-12-17 11:41:54', '2021-12-17 11:41:54'),
(19, 'add_users', 'users', '2021-12-17 11:41:54', '2021-12-17 11:41:54'),
(20, 'delete_users', 'users', '2021-12-17 11:41:54', '2021-12-17 11:41:54'),
(21, 'browse_settings', 'settings', '2021-12-17 11:41:54', '2021-12-17 11:41:54'),
(22, 'read_settings', 'settings', '2021-12-17 11:41:54', '2021-12-17 11:41:54'),
(23, 'edit_settings', 'settings', '2021-12-17 11:41:54', '2021-12-17 11:41:54'),
(24, 'add_settings', 'settings', '2021-12-17 11:41:55', '2021-12-17 11:41:55'),
(25, 'delete_settings', 'settings', '2021-12-17 11:41:55', '2021-12-17 11:41:55'),
(26, 'browse_bundles', 'bundles', '2021-12-19 21:54:15', '2021-12-19 21:54:15'),
(27, 'read_bundles', 'bundles', '2021-12-19 21:54:15', '2021-12-19 21:54:15'),
(28, 'edit_bundles', 'bundles', '2021-12-19 21:54:15', '2021-12-19 21:54:15'),
(29, 'add_bundles', 'bundles', '2021-12-19 21:54:15', '2021-12-19 21:54:15'),
(30, 'delete_bundles', 'bundles', '2021-12-19 21:54:15', '2021-12-19 21:54:15'),
(31, 'browse_sections', 'sections', '2021-12-19 22:23:11', '2021-12-19 22:23:11'),
(32, 'read_sections', 'sections', '2021-12-19 22:23:11', '2021-12-19 22:23:11'),
(33, 'edit_sections', 'sections', '2021-12-19 22:23:11', '2021-12-19 22:23:11'),
(34, 'add_sections', 'sections', '2021-12-19 22:23:11', '2021-12-19 22:23:11'),
(35, 'delete_sections', 'sections', '2021-12-19 22:23:11', '2021-12-19 22:23:11'),
(36, 'browse_suppliers', 'suppliers', '2021-12-19 23:06:40', '2021-12-19 23:06:40'),
(37, 'read_suppliers', 'suppliers', '2021-12-19 23:06:40', '2021-12-19 23:06:40'),
(38, 'edit_suppliers', 'suppliers', '2021-12-19 23:06:40', '2021-12-19 23:06:40'),
(39, 'add_suppliers', 'suppliers', '2021-12-19 23:06:40', '2021-12-19 23:06:40'),
(40, 'delete_suppliers', 'suppliers', '2021-12-19 23:06:40', '2021-12-19 23:06:40'),
(41, 'browse_deals', 'deals', '2021-12-20 08:12:08', '2021-12-20 08:12:08'),
(42, 'read_deals', 'deals', '2021-12-20 08:12:08', '2021-12-20 08:12:08'),
(43, 'edit_deals', 'deals', '2021-12-20 08:12:08', '2021-12-20 08:12:08'),
(44, 'add_deals', 'deals', '2021-12-20 08:12:08', '2021-12-20 08:12:08'),
(45, 'delete_deals', 'deals', '2021-12-20 08:12:08', '2021-12-20 08:12:08'),
(46, 'browse_customers', 'customers', '2021-12-20 08:46:04', '2021-12-20 08:46:04'),
(47, 'read_customers', 'customers', '2021-12-20 08:46:04', '2021-12-20 08:46:04'),
(48, 'edit_customers', 'customers', '2021-12-20 08:46:04', '2021-12-20 08:46:04'),
(49, 'add_customers', 'customers', '2021-12-20 08:46:04', '2021-12-20 08:46:04'),
(50, 'delete_customers', 'customers', '2021-12-20 08:46:04', '2021-12-20 08:46:04'),
(51, 'browse_general_maintainance_requests', 'general_maintainance_requests', '2021-12-20 08:57:45', '2021-12-20 08:57:45'),
(52, 'read_general_maintainance_requests', 'general_maintainance_requests', '2021-12-20 08:57:45', '2021-12-20 08:57:45'),
(53, 'edit_general_maintainance_requests', 'general_maintainance_requests', '2021-12-20 08:57:45', '2021-12-20 08:57:45'),
(54, 'add_general_maintainance_requests', 'general_maintainance_requests', '2021-12-20 08:57:45', '2021-12-20 08:57:45'),
(55, 'delete_general_maintainance_requests', 'general_maintainance_requests', '2021-12-20 08:57:45', '2021-12-20 08:57:45'),
(56, 'browse_mobile_maintenance_requests', 'mobile_maintenance_requests', '2021-12-20 09:23:34', '2021-12-20 09:23:34'),
(57, 'read_mobile_maintenance_requests', 'mobile_maintenance_requests', '2021-12-20 09:23:34', '2021-12-20 09:23:34'),
(58, 'edit_mobile_maintenance_requests', 'mobile_maintenance_requests', '2021-12-20 09:23:34', '2021-12-20 09:23:34'),
(59, 'add_mobile_maintenance_requests', 'mobile_maintenance_requests', '2021-12-20 09:23:34', '2021-12-20 09:23:34'),
(60, 'delete_mobile_maintenance_requests', 'mobile_maintenance_requests', '2021-12-20 09:23:34', '2021-12-20 09:23:34'),
(61, 'browse_cameras', 'cameras', '2021-12-20 09:27:38', '2021-12-20 09:27:38'),
(62, 'read_cameras', 'cameras', '2021-12-20 09:27:38', '2021-12-20 09:27:38'),
(63, 'edit_cameras', 'cameras', '2021-12-20 09:27:38', '2021-12-20 09:27:38'),
(64, 'add_cameras', 'cameras', '2021-12-20 09:27:38', '2021-12-20 09:27:38'),
(65, 'delete_cameras', 'cameras', '2021-12-20 09:27:38', '2021-12-20 09:27:38'),
(66, 'browse_camera_appointments', 'camera_appointments', '2021-12-20 09:32:35', '2021-12-20 09:32:35'),
(67, 'read_camera_appointments', 'camera_appointments', '2021-12-20 09:32:35', '2021-12-20 09:32:35'),
(68, 'edit_camera_appointments', 'camera_appointments', '2021-12-20 09:32:35', '2021-12-20 09:32:35'),
(69, 'add_camera_appointments', 'camera_appointments', '2021-12-20 09:32:35', '2021-12-20 09:32:35'),
(70, 'delete_camera_appointments', 'camera_appointments', '2021-12-20 09:32:35', '2021-12-20 09:32:35'),
(81, 'browse_sticker_info', 'sticker_info', '2021-12-20 09:48:37', '2021-12-20 09:48:37'),
(82, 'read_sticker_info', 'sticker_info', '2021-12-20 09:48:37', '2021-12-20 09:48:37'),
(83, 'edit_sticker_info', 'sticker_info', '2021-12-20 09:48:37', '2021-12-20 09:48:37'),
(84, 'add_sticker_info', 'sticker_info', '2021-12-20 09:48:37', '2021-12-20 09:48:37'),
(85, 'delete_sticker_info', 'sticker_info', '2021-12-20 09:48:37', '2021-12-20 09:48:37'),
(91, 'browse_sticker_barcode_infos', 'sticker_barcode_infos', '2021-12-20 10:01:07', '2021-12-20 10:01:07'),
(92, 'read_sticker_barcode_infos', 'sticker_barcode_infos', '2021-12-20 10:01:07', '2021-12-20 10:01:07'),
(93, 'edit_sticker_barcode_infos', 'sticker_barcode_infos', '2021-12-20 10:01:07', '2021-12-20 10:01:07'),
(94, 'add_sticker_barcode_infos', 'sticker_barcode_infos', '2021-12-20 10:01:08', '2021-12-20 10:01:08'),
(95, 'delete_sticker_barcode_infos', 'sticker_barcode_infos', '2021-12-20 10:01:08', '2021-12-20 10:01:08'),
(96, 'browse_notifications', 'notifications', '2021-12-24 21:53:05', '2021-12-24 21:53:05'),
(97, 'read_notifications', 'notifications', '2021-12-24 21:53:05', '2021-12-24 21:53:05'),
(98, 'edit_notifications', 'notifications', '2021-12-24 21:53:06', '2021-12-24 21:53:06'),
(99, 'add_notifications', 'notifications', '2021-12-24 21:53:06', '2021-12-24 21:53:06'),
(100, 'delete_notifications', 'notifications', '2021-12-24 21:53:06', '2021-12-24 21:53:06');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(16, 1),
(16, 3),
(17, 1),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 3),
(26, 1),
(26, 3),
(27, 1),
(27, 3),
(28, 1),
(28, 3),
(29, 1),
(29, 3),
(30, 1),
(30, 3),
(31, 1),
(31, 3),
(32, 1),
(32, 3),
(33, 1),
(33, 3),
(34, 1),
(34, 3),
(35, 1),
(35, 3),
(36, 1),
(36, 3),
(37, 1),
(37, 3),
(38, 1),
(38, 3),
(39, 1),
(39, 3),
(40, 1),
(40, 3),
(41, 1),
(41, 3),
(42, 1),
(42, 3),
(43, 1),
(43, 3),
(44, 1),
(44, 3),
(45, 1),
(45, 3),
(46, 1),
(46, 3),
(47, 1),
(47, 3),
(48, 1),
(48, 3),
(49, 1),
(49, 3),
(50, 1),
(50, 3),
(51, 1),
(51, 3),
(52, 1),
(52, 3),
(53, 1),
(53, 3),
(54, 1),
(54, 3),
(55, 3),
(56, 1),
(56, 3),
(57, 1),
(57, 3),
(58, 1),
(58, 3),
(59, 1),
(59, 3),
(60, 3),
(61, 1),
(61, 3),
(62, 1),
(62, 3),
(63, 1),
(63, 3),
(64, 1),
(64, 3),
(65, 1),
(65, 3),
(66, 1),
(66, 3),
(67, 1),
(67, 3),
(68, 1),
(68, 3),
(69, 1),
(69, 3),
(70, 3),
(81, 1),
(81, 3),
(82, 1),
(82, 3),
(83, 1),
(83, 3),
(84, 1),
(84, 3),
(85, 1),
(85, 3),
(91, 1),
(91, 3),
(92, 1),
(92, 3),
(93, 1),
(93, 3),
(94, 1),
(94, 3),
(95, 1),
(95, 3),
(96, 1),
(96, 3),
(97, 1),
(97, 3),
(99, 1),
(99, 3);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 4, 'LaravelSanctumAuth', '2d26759fcba9c6d41d75684b4256ffb65940bd7727561f62d9c7e7094f5818fd', '[\"*\"]', NULL, '2021-12-22 14:56:50', '2021-12-22 14:56:50'),
(2, 'App\\Models\\User', 5, 'LaravelSanctumAuth', '1e6bbc394ae6cd97b2741b80e6ac7526f5d0967e844bfb9aec24de770f412fff', '[\"*\"]', NULL, '2021-12-22 14:57:28', '2021-12-22 14:57:28'),
(3, 'App\\Models\\User', 6, 'LaravelSanctumAuth', '5690497c3782cf1c70f04c8770cde75b1f420d69585104df9df7b881d9b3112e', '[\"*\"]', NULL, '2021-12-22 14:58:42', '2021-12-22 14:58:42'),
(4, 'App\\Models\\User', 7, 'LaravelSanctumAuth', '28f3c39a1e7b28e1e6298e9fdf79ff4f2a64f0b8b2f7f0e79e334a0cbba720d7', '[\"*\"]', NULL, '2021-12-22 14:59:15', '2021-12-22 14:59:15'),
(5, 'App\\Models\\User', 7, 'LaravelSanctumAuth', '96a671a3cc9bcf0544aabcc2b607337a1ba7331a7bdeef84b41db3a180313cf5', '[\"*\"]', NULL, '2021-12-22 15:21:52', '2021-12-22 15:21:52'),
(6, 'App\\Models\\User', 7, 'LaravelSanctumAuth', 'b638fe1f5964aefb24a0fd5e1cdabfecb9418305e7d274592b7b58fd480ebc3f', '[\"*\"]', '2021-12-22 15:38:55', '2021-12-22 15:22:08', '2021-12-22 15:38:55'),
(7, 'App\\Models\\User', 7, 'LaravelSanctumAuth', 'be0d5dbadff6cae4851ea21beb4ab85199733499907bcd86928702b3668fea1b', '[\"*\"]', '2021-12-22 15:48:04', '2021-12-22 15:39:11', '2021-12-22 15:48:04'),
(8, 'App\\Models\\User', 7, 'LaravelSanctumAuth', '9e3f72c629d6a0c3f84ba7d789934f463e59228cfdd5914fdfe39b11ce2201e2', '[\"*\"]', NULL, '2021-12-22 15:48:19', '2021-12-22 15:48:19'),
(9, 'App\\Models\\User', 7, 'LaravelSanctumAuth', '26db36dae05335b47693e1d0862df2041ff5419d12791663a74734e472c57aab', '[\"*\"]', NULL, '2021-12-22 16:01:20', '2021-12-22 16:01:20'),
(10, 'App\\Models\\User', 7, 'LaravelSanctumAuth', '0115e4d931cb592102ed7bdb313255138401e1e5cd8ae7b0809e962c5775a1f0', '[\"*\"]', NULL, '2021-12-22 16:02:49', '2021-12-22 16:02:49'),
(11, 'App\\Models\\User', 7, 'LaravelSanctumAuth', '0fb283eca1ddc21b6ae1923a996cd43c605fdbc44510a585820f9c2692c28552', '[\"*\"]', NULL, '2021-12-22 16:04:09', '2021-12-22 16:04:09'),
(12, 'App\\Models\\User', 7, 'LaravelSanctumAuth', '8722224d7cd7425497bd4b6f56a36d0b6fe7de4918e95f771ae7aab58a40ac65', '[\"*\"]', NULL, '2021-12-22 16:05:14', '2021-12-22 16:05:14'),
(13, 'App\\Models\\User', 7, 'LaravelSanctumAuth', '6ba94345ca9467c57fbf153a3dbeb6bab3424a351415eeaf8537477fc52ad5dd', '[\"*\"]', NULL, '2021-12-22 16:05:50', '2021-12-22 16:05:50'),
(14, 'App\\Models\\User', 7, 'LaravelSanctumAuth', '1f876b19860cc2b787182d57472bf1ddf42cf2fb467cc410162330eaca3c0043', '[\"*\"]', NULL, '2021-12-22 16:13:42', '2021-12-22 16:13:42'),
(15, 'App\\Models\\User', 9, 'LaravelSanctumAuth', '18ff821651e9ad9cc49260343720791ef67acd1bce36074c37d8b4eeaf0a3d87', '[\"*\"]', NULL, '2021-12-22 16:18:00', '2021-12-22 16:18:00'),
(16, 'App\\Models\\User', 9, 'LaravelSanctumAuth', '3291af779a23cfda4f59f00b57a4123a362b20b3b98b5b523ba0d3589826a693', '[\"*\"]', '2021-12-22 16:24:10', '2021-12-22 16:22:55', '2021-12-22 16:24:10'),
(18, 'App\\Models\\User', 9, 'LaravelSanctumAuth', '8a8791138fe272b9171acb4749e8258269d3196e4bb8b47c077c651dd48b54d8', '[\"*\"]', NULL, '2021-12-22 16:27:41', '2021-12-22 16:27:41'),
(20, 'App\\Models\\User', 9, 'LaravelSanctumAuth', '9b337845d5466ba4bc4110d8086ee92c9f340a2ed5077cfd97c91e23f35d5016', '[\"*\"]', '2021-12-22 18:37:58', '2021-12-22 16:51:03', '2021-12-22 18:37:58'),
(21, 'App\\Models\\User', 10, 'LaravelSanctumAuth', '36ff7f75f9be9df043686901a6a08b45f93cc00818041fc164764a624e4fc60e', '[\"*\"]', NULL, '2021-12-22 18:40:21', '2021-12-22 18:40:21'),
(22, 'App\\Models\\User', 10, 'LaravelSanctumAuth', '86a4e5e1533d8c10262a01f8a51704a277b25cc031531357ba8dd05fe6af704b', '[\"*\"]', NULL, '2021-12-22 18:40:38', '2021-12-22 18:40:38'),
(23, 'App\\Models\\User', 10, 'LaravelSanctumAuth', '47643ba4f90cd378bfe302cceb5ee2933d2afb216465e62950a7f436bba3eaa1', '[\"*\"]', '2021-12-24 14:49:21', '2021-12-22 18:40:47', '2021-12-24 14:49:21'),
(24, 'App\\Models\\User', 10, 'LaravelSanctumAuth', '998f87aae8b618fd9df23bbfdb0ae1e7b7eaadc2839f6194771684f3379bea22', '[\"*\"]', '2021-12-24 10:41:40', '2021-12-23 20:14:19', '2021-12-24 10:41:40'),
(25, 'App\\Models\\User', 10, 'LaravelSanctumAuth', '142c19b9eb86d715543426a53e422d695c77756a85381b4d186e07fb104736f0', '[\"*\"]', NULL, '2021-12-23 20:15:52', '2021-12-23 20:15:52'),
(26, 'App\\Models\\User', 11, 'LaravelSanctumAuth', '48255115d39f06a2079b56734940b9d82a615369c22bcc1436966cc96fdbc952', '[\"*\"]', '2021-12-24 14:04:26', '2021-12-24 13:55:20', '2021-12-24 14:04:26'),
(27, 'App\\Models\\User', 11, 'LaravelSanctumAuth', '005078fc02146b24e50dc6b86029b944752b130e1283b7679fce2f51812a3cb9', '[\"*\"]', '2021-12-24 14:39:50', '2021-12-24 14:39:18', '2021-12-24 14:39:50'),
(28, 'App\\Models\\User', 11, 'LaravelSanctumAuth', '33349953907ca66c0dfedb6a4be6d2afcd0828da160ee9385d3bb018ee8e4da7', '[\"*\"]', '2022-01-10 08:09:39', '2021-12-24 15:56:25', '2022-01-10 08:09:39'),
(29, 'App\\Models\\User', 11, 'view-token', 'f93b660928e83bcc27e7bce36ab2e141edb6917d4afa9ff4ff99460ab89c7a14', '[\"*\"]', '2022-01-06 09:35:06', '2021-12-24 22:12:33', '2022-01-06 09:35:06'),
(30, 'App\\Models\\User', 12, 'view-token', 'c67207cc146555595637354998b7a2818a86e801fa2a2c616e2832513bb9b190', '[\"*\"]', '2022-01-06 09:20:03', '2022-01-06 08:54:54', '2022-01-06 09:20:03'),
(31, 'App\\Models\\User', 12, 'view-token', 'c3e16d798a949eca2cc0b14c1a263f0b7751d556e95edc051c6b7d2d1a02b8f2', '[\"*\"]', NULL, '2022-01-06 09:20:49', '2022-01-06 09:20:49'),
(32, 'App\\Models\\User', 13, 'view-token', '031ae35ad669b19b771723e9df3cd90422039928eadd013482745a494008e6c8', '[\"*\"]', NULL, '2022-01-06 09:21:21', '2022-01-06 09:21:21'),
(33, 'App\\Models\\User', 14, 'view-token', '9eb579634311851733d0dcf5cf75e6702e93d86b03ebd4f9d5e503052062059f', '[\"*\"]', '2022-01-06 09:26:37', '2022-01-06 09:23:05', '2022-01-06 09:26:37'),
(34, 'App\\Models\\User', 15, 'view-token', '4386e40ec2cd605dffed61c92cf40c12723c4c00d7072fb2be373d99d518fa2c', '[\"*\"]', NULL, '2022-01-06 09:27:01', '2022-01-06 09:27:01'),
(35, 'App\\Models\\User', 16, 'view-token', '9e0f06acd79007693629238da371cb0016d0143f193f5e0f5f42209294e5721d', '[\"*\"]', NULL, '2022-01-06 09:27:46', '2022-01-06 09:27:46'),
(36, 'App\\Models\\User', 17, 'view-token', '418e81de5f5cf26f9a8c295d8cce49c0e2b8326d63e61f9bd79073bb617a2b18', '[\"*\"]', NULL, '2022-01-06 09:29:32', '2022-01-06 09:29:32'),
(37, 'App\\Models\\User', 18, 'view-token', '56a2f0efa803fa38a09e388b3fcc25cdb514edcfaa7395176f5c884e0d6ae808', '[\"*\"]', '2022-01-06 09:34:01', '2022-01-06 09:33:37', '2022-01-06 09:34:01'),
(38, 'App\\Models\\User', 12, 'view-token', 'cd36f7b327b59237f35b8628d8316dd79365aa565f270b9995db9816ec9f4428', '[\"*\"]', NULL, '2022-01-06 09:34:56', '2022-01-06 09:34:56');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2021-12-17 11:41:52', '2021-12-17 11:41:52'),
(2, 'user', 'Normal User', '2021-12-17 11:41:52', '2021-12-17 11:41:52'),
(3, 'super admin', 'Super Admin', '2022-01-06 14:35:32', '2022-01-06 14:35:32');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `name`, `description`, `location`, `contact_number`, `email`, `contact_name`) VALUES
(2, 'haidarjbeily', 'qwqw', '22,22', '0997610306', 'haidarjbeily76@gmail.com', 'Haidar Jbeily');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings/December2021/nEa4rRNPdpMq06SIq5bM.png', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'View', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to View Dashboard', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings/December2021/bLeOnOYfUR34Lynw8MYt.png', '', 'image', 4, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `name`, `description`, `code`) VALUES
(1, 'Pending', NULL, 'pending'),
(2, 'In Progres', NULL, 'in progress'),
(3, 'Completed', NULL, 'completed');

-- --------------------------------------------------------

--
-- Table structure for table `sticker_barcode_infos`
--

CREATE TABLE `sticker_barcode_infos` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `applied_at` timestamp NULL DEFAULT NULL,
  `expired_at` timestamp NULL DEFAULT NULL,
  `device_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` float NOT NULL,
  `price` float NOT NULL,
  `warranty_days` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sticker_barcode_infos`
--

INSERT INTO `sticker_barcode_infos` (`id`, `customer_id`, `applied_at`, `expired_at`, `device_type`, `serial_number`, `image`, `name`, `description`, `model`, `cost`, `price`, `warranty_days`) VALUES
(51, 8, '2022-01-09 21:49:00', '2022-01-27 21:49:00', 'wad', '1232', '/barcodes/51.png', 'test', 'dsa', 'dd', 12, 1212, 0);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_goods` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount_available` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `contact_first_name`, `contact_last_name`, `address`, `city`, `postal_code`, `country`, `phone`, `email`, `fax`, `url`, `type_goods`, `notes`, `logo`, `discount_available`) VALUES
(2, 'haidarjbeily76@gmail.com', 'Haidar', 'Jbeily', 'qhh', 'Abu Dhabi', 111, 'Syria', '997610306', 'haidarjbeily76@gmail.com', NULL, '/admin/sections', NULL, '111', 'suppliers\\December2021\\jZkKRFbl2i397CTTCVJB.jpg', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `phone`) VALUES
(1, 3, 'Qadmin', 'qtech@qtech.com', 'users/December2021/sY1hnigh6seFrHoXYrLf.png', NULL, '$2y$10$MzBA2XXV1WYalL8ms7n46OzVmuCGBFTBUD0tawp06aspNkdFecZaa', NULL, '{\"locale\":\"en\"}', '2021-12-17 11:55:29', '2021-12-21 22:30:06', NULL),
(7, 2, 'haidar', 'haidar@haidar.com', NULL, NULL, '$2y$10$WMdjMsQ.s9Q.tHFcJBQe1.FXYFdGkEHa648mwxnJcD.FBmIBIGymy', NULL, NULL, '2021-12-22 14:59:15', '2021-12-22 14:59:15', '0997610306'),
(8, 2, 'haidar', 'haidar1@haida1r.com', NULL, NULL, '$2y$10$4eksLDB2mQKloG/bdaYaDea31oHHv5bDqzeJStdFjtT3Drc2vsGpq', NULL, NULL, '2021-12-22 16:16:21', '2021-12-22 16:16:22', '0987610306'),
(9, 2, 'haidar11', 'haidar11@haida1r.com', NULL, NULL, '$2y$10$iqTMkmRWXWRusD36O.XapuZxueJM5SZ5ZutTwITzH/R9.7e0hj/fm', NULL, NULL, '2021-12-22 16:18:00', '2021-12-22 16:18:00', '0988610306'),
(10, 2, 'HaidarJbeily', 'haidar@test.com', NULL, NULL, '$2y$10$WQImJ1JF2U2.YfDruMBkTegjanoYQhiQ9qluz4ZjYQlRbqfdfALIG', NULL, NULL, '2021-12-22 18:40:21', '2021-12-22 18:40:21', '0998877661'),
(11, 2, 'Haidar1Jbeily', 'haidarjbeily@gg.com', NULL, NULL, '$2y$10$20GsIF9k5ZwDmDA/AgwrQ.mnPlmtixI7dLCbcgW.mp6Jdn5ltgZhy', NULL, NULL, '2021-12-24 13:55:20', '2022-01-04 14:28:39', '09948877661'),
(12, 2, 'Haidar1Jb', 'haidar1@test.com', NULL, NULL, '$2y$10$EjjJp1gtAVDbswEerx2feO7gG0mI/kJxc0D2sclyTHkvefcQYC1by', NULL, NULL, '2022-01-06 08:54:53', '2022-01-06 08:54:53', '0994887766'),
(13, 2, 'Haidar1Jb1', 'haidar11@test.com', NULL, NULL, '$2y$10$NppnSQmytWgl7Vb1OcwE1.kiLQGgG9AN8SUROSaO9GZddShfheSxq', NULL, NULL, '2022-01-06 09:21:21', '2022-01-06 09:21:21', '0994887764'),
(14, 2, 'Haidar1Jb11', 'haidar111@test.com', NULL, NULL, '$2y$10$463DXa0bShdhihrq62hyg.QHGj1q1hHE.qxujdwzq2ICKqUwFEqly', NULL, NULL, '2022-01-06 09:23:05', '2022-01-06 09:23:05', '0994887762'),
(15, 2, 'Haidar1Jb10', 'haidar109@test.com', NULL, NULL, '$2y$10$7BkHBTOSKX6R9AjdwljNsuV47AnEQLoBAAb5D4NHm4lQlXzWt8SZ6', NULL, NULL, '2022-01-06 09:27:01', '2022-01-06 09:27:01', '0994887765'),
(16, 2, 'Haidar12', 'haidar9@test.com', NULL, NULL, '$2y$10$0jktpqgTzJIgOMVdcgohwuCowTSVjccsJTn6OmZzhIKWi3dwKqwyy', NULL, NULL, '2022-01-06 09:27:45', '2022-01-06 09:27:46', '0994887763'),
(17, 2, 'Haidar120', 'haidar0@test.com', NULL, NULL, '$2y$10$o6LCIZk3t1b3sqNAPUf3Muh0NnQK26MGT4BnMJEoGIK2P2sATJBHa', NULL, NULL, '2022-01-06 09:29:32', '2022-01-06 09:29:32', '09948877612'),
(18, 2, 'yy', 'haidary@test.com', NULL, NULL, '$2y$10$Iq5RAXt8zy7FuaPUT3ac7OcEmEPpKYEHEi2M6APlChyJ0XF8KZReW', NULL, NULL, '2022-01-06 09:33:37', '2022-01-06 09:33:37', '0999999991'),
(19, 1, 'admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$EUvysCTwCKDLqz5hf22.Ju6ljKAwDPha.bBlNkGxk6lhsJx3gPM3S', NULL, '{\"locale\":\"en\"}', '2022-01-07 13:02:58', '2022-01-07 13:02:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bundles`
--
ALTER TABLE `bundles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bundles_supplier_id_index` (`supplier_id`),
  ADD KEY `bundles_section_id_index` (`section_id`);

--
-- Indexes for table `cameras`
--
ALTER TABLE `cameras`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cameras_model_unique` (`model`);

--
-- Indexes for table `camera_appointments`
--
ALTER TABLE `camera_appointments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `camera_appointments_customer_id_index` (`customer_id`),
  ADD KEY `camera_appointments_camera_id_index` (`camera_id`),
  ADD KEY `camera_appointments_status_id_index` (`status_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_username_unique` (`username`),
  ADD KEY `customers_user_id_index` (`user_id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `deals`
--
ALTER TABLE `deals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `general_maintainance_requests`
--
ALTER TABLE `general_maintainance_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `general_maintainance_requests_customer_id_index` (`customer_id`),
  ADD KEY `general_maintainance_requests_status_id_index` (`status_id`);

--
-- Indexes for table `guarantee_types`
--
ALTER TABLE `guarantee_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guarantee_types_sticker_info`
--
ALTER TABLE `guarantee_types_sticker_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `guarantee_types_sticker_info_guarantee_type_id_index` (`guarantee_type_id`),
  ADD KEY `guarantee_types_sticker_info_sticker_info_id_index` (`sticker_info_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile_maintenance_requests`
--
ALTER TABLE `mobile_maintenance_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mobile_maintenance_requests_customer_id_index` (`customer_id`),
  ADD KEY `mobile_maintenance_requests_status_id_index` (`status_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_user_id_index` (`customer_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sticker_barcode_infos`
--
ALTER TABLE `sticker_barcode_infos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sticker_barcode_info_customer_id_index` (`customer_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bundles`
--
ALTER TABLE `bundles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cameras`
--
ALTER TABLE `cameras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `camera_appointments`
--
ALTER TABLE `camera_appointments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `deals`
--
ALTER TABLE `deals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `general_maintainance_requests`
--
ALTER TABLE `general_maintainance_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `guarantee_types`
--
ALTER TABLE `guarantee_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `guarantee_types_sticker_info`
--
ALTER TABLE `guarantee_types_sticker_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `mobile_maintenance_requests`
--
ALTER TABLE `mobile_maintenance_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sticker_barcode_infos`
--
ALTER TABLE `sticker_barcode_infos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
