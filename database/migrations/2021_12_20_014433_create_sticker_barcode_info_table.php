<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStickerBarcodeInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sticker_barcode_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->index();
            $table->unsignedInteger('sticker_id')->index();
            $table->timestamp('applied_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->string('device_type');
            $table->string('serial_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sticker_barcode_info');
    }
}
