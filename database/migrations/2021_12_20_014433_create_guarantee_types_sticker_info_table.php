<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuaranteeTypesStickerInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guarantee_types_sticker_info', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('guarantee_type_id')->index();
            $table->unsignedInteger('sticker_info_id')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guarantee_types_sticker_info');
    }
}
