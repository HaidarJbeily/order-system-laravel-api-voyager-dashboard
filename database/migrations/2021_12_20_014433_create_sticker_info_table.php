<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStickerInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sticker_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 20);
            $table->string('description')->nullable();
            $table->string('model')->unique();
            $table->float('cost', 10, 0);
            $table->float('price', 10, 0);
            $table->integer('changes_number');
            $table->integer('warranty_days');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sticker_info');
    }
}
