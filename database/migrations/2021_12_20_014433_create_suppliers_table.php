<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('contact_first_name')->nullable();
            $table->string('contact_last_name')->nullable();
            $table->string('address');
            $table->string('city');
            $table->integer('postal_code')->nullable();
            $table->string('country');
            $table->integer('phone')->nullable();
            $table->string('email');
            $table->integer('fax')->nullable();
            $table->string('url');
            $table->string('type_goods')->nullable();
            $table->string('notes')->nullable();
            $table->string('logo');
            $table->boolean('discount_available')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
