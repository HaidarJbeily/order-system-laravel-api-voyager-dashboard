<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->integer('rank')->nullable();
            $table->string('location');
            $table->string('username')->unique();
            $table->string('image')->nullable();
            $table->string('bio')->nullable();
            $table->integer('address');
            $table->date('dob')->nullable();
            $table->string('city', 25)->nullable();
            $table->integer('mobile_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
