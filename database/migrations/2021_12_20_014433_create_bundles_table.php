<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBundlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bundles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->unsignedInteger('supplier_id')->index();
            $table->unsignedInteger('section_id')->index();
            $table->integer('price');
            $table->integer('bundle_size');
            $table->string('picture')->nullable();
            $table->integer('ranking')->nullable();
            $table->string('note')->nullable();
            $table->boolean('discount_available');
            $table->boolean('is_available');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bundles');
    }
}
