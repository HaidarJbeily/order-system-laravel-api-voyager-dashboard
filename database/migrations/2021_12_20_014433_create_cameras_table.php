<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCamerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cameras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model')->unique();
            $table->string('sensor')->nullable();
            $table->string('resolution')->nullable();
            $table->float('cost', 10, 0);
            $table->float('price', 10, 0);
            $table->string('manufacturer');
            $table->integer('rank')->nullable();
            $table->boolean('is_available')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cameras');
    }
}
